#!/bin/bash

# Author: Tiago Epifanio
# Developed for: https://tilde.pt
# Reads banners from all users home folders, joins them with a logo file
#  and saves everything into the specified output directory
# Banners size: 80x8 characters

homebase=/home #location of the home folders
logofile=./logo.txt #location of the logo file
inputdir_name=banners #read the banners from this dir inside users' home
outdir=./tildebanners #location of the output dir

if [ ! -d "$outdir" ]; then
    mkdir "$outdir"
fi

rm -f "$outdir"/*

count=1

for bannerfile in $(find $homebase/*/$inputdir_name -maxdepth 2 -type f)
do
    lines=$(wc -l $bannerfile | awk '{ print $1 }')
    cols=$(wc -L $bannerfile | awk '{ print $1 }')

    if [[ $lines -le 8 && $cols -le 80 ]]
    then
        paste -d ' ' $logofile $bannerfile > "$outdir/$count".txt
        ((count++))
    fi
done
